all: deps

deps:
	rebar get-deps
	rebar compile

app:
	rebar compile

tests:
	rebar eunit

clean:
	rm -f -- ebin/*.beam Emakefile ebin/$(APP).app

ebin/$(APP).app: src/$(APP).app.src
	mkdir -p ebin
	cp -f -- $< $@

ifdef DEBUG
EXTRA_OPTS:=debug_info,
endif

ifdef TEST
EXTRA_OPTS:=$(EXTRA_OPTS) {d,'TEST', true},
endif

Emakefile: Emakefile.src
	sed "s/{{EXTRA_OPTS}}/$(EXTRA_OPTS)/" $< > $@

.PHONY: all deps app tests clean distclean
